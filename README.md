-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Constitution of the Free Software Foundation Europe

The versions with the SHA1 checksums

 4af94622a110c0cea6db9496d6bc97c87e33424a  Constitution.de.tex
 522dfb7e3eb19e9dc299155d558ed1891adf0198  Constitution.en.tex

were accepted on 9 October 2009 as the new version of the Constitution
by an Extraordinary General Assembly and are the currently binding version.

                                Jonas Öberg
                                Gnesta, Sweden
                                25 January 2018
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIVAwUBWmmsfirOf24j9/+tAQjlzw/9E9i0EhpEV5r9m7MHxMFqkG9+1YmAYl8l
84Uqzl7YBqmUfeaFw1iTGhKcx4t6n2NFA9x7XzoWpJXgpSpxosjxoI2e1PK6Gxv0
UuN/cklnc0z/laD6vj2aiwu1PVZlDaNuelKrZv8dOYOe4Ya9TPI0o2EX4cMP0U4J
GdrCoGHjL4GkLalCl09xIGtrjxwD2GCkKWgMtCOUJMB6TfA0TmElezZXQ9JSCOu+
dBI5xUQKspRxShpExmGFt3gyiyMoaEQIUg14wMM3Q4QW4W0Ww6At81OOKdx+zO8+
wgw4UHLofhFniEQ4w1Xnx7flhmAtS0KaZSRXMGUpDRvSBStK0N1EtLxCD0vnvI/1
fgx0EPfI1GtJ3C/QGjj3OR/qz8lvrDFHVqzTinOFzuo8qEHm/wbCVb+V2KjEzhfq
/h0+e1XbqXAcHfTugg1UCOnKZ/iQnBgmjnJ+sF9rupgb1HIgpGsBHHYULAjKxaMb
6706SmM6ZiyT74cHsU0QmZsrnGacUNkLOr4aDE9QkurE0E5LTLrnWAJFTIN4ZFsu
qJ7P5dQYRTN4VfTpIS7kMI3LrxpbCNL1aS7H85I04pVc4PqUs/Uz+F4VtcTJYMVK
yjoeSNUUD5zwf2hpUJvvvdWvhujK0RcvuIk7eB+hcwI9EoXud/BttjsyYeb0zRDp
oWv5nfyJcqY=
=h5Py
-----END PGP SIGNATURE-----
